﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Models;
using api_rest.Extensions;
using api_rest.Resources;
using AutoMapper;
using api_rest.Domain.Helpers;

namespace api_rest.Mapping
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Companhia, CompanhiaResource>();

            CreateMap<Usuario, UsuarioResource>();

            CreateMap<Produto, ProdutoResource>();

                /*.ForMember(src => src.FormaPagamento,
                           opt => opt.MapFrom(src => src.FormaPagamento.ToDescriptionString()));*/
        }
    }
}
