﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using api_rest.Domain.Models;
using api_rest.Domain.Helpers;


namespace api_rest.Persistence.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<Companhia> Categories { get; set; }
        public DbSet<Produto> Products { get; set; }

        public DbSet<Usuario> Users { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Companhia>().ToTable("Categories");
            builder.Entity<Companhia>().HasKey(p => p.Id);
            builder.Entity<Companhia>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Companhia>().Property(p => p.NomeFantasia).IsRequired().HasMaxLength(30);
            builder.Entity<Companhia>().HasMany(p => p.Products).WithOne(p => p.Companhia).HasForeignKey(p => p.CompanhiaId);

            builder.Entity<Companhia>().HasData
            (
                new Companhia { Id = 100, NomeFantasia = "Fruits and Vegetables" }, // Id set manually due to in-memory provider
                new Companhia { Id = 101, NomeFantasia = "Dairy" }
            );

            builder.Entity<Produto>().ToTable("Products");
            builder.Entity<Produto>().HasKey(p => p.Id);
            builder.Entity<Produto>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Produto>().Property(p => p.Nome).IsRequired().HasMaxLength(50);
            builder.Entity<Produto>().Property(p => p.Valor).IsRequired();
            //builder.Entity<Produto>().Property(p => p.Companhia.Id).IsRequired();

            builder.Entity<Produto>().HasData
            (
                new Produto
                {
                    Id = 100,
                    Nome = "Apple",
                    Valor = 1,
                    Observacao = "Big Apple",
                    CompanhiaId = 100
                },
                new Produto
                {
                    Id = 101,
                    Nome = "Milk",
                    Valor = 2,
                    Observacao = "Skimmed Milk",
                    CompanhiaId = 101,
                }
            );

            builder.Entity<Usuario>().ToTable("Users");
            builder.Entity<Usuario>().HasKey(p => p.Id);
            builder.Entity<Usuario>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Usuario>().Property(p => p.Senha).IsRequired().HasMaxLength(50);
            builder.Entity<Usuario>().Property(p => p.Email).IsRequired().HasMaxLength(10);
            
            builder.Entity<Usuario>().HasData
            (
                new Usuario
                {
                    Id = 100,
                    Email = "john@mail.com",
                    Senha = "12345",
                }
            );
        }
    }
}
