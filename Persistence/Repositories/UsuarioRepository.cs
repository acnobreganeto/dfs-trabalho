﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Repositories;
using api_rest.Domain.Models;
using api_rest.Persistence.Context;
using Microsoft.EntityFrameworkCore;

namespace api_rest.Persistence.Repositories
{
    public class UsuarioRepository : BaseRepository, IUserRepository
    {
        public UsuarioRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Usuario> FirstOrDefaultAsync(String email, String senha)
        {
            return await _context.Users.FirstOrDefaultAsync(x => x.Email == email && x.Senha == senha);
        }
    
    }

}
