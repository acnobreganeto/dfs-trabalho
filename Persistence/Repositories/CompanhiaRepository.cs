﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Repositories;
using api_rest.Domain.Models;
using api_rest.Persistence.Context;
using Microsoft.EntityFrameworkCore;

namespace api_rest.Persistence.Repositories
{
    public class CompanhiaRepository : BaseRepository, ICategoryRepository
    {
        public CompanhiaRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Companhia>> ListAsync()
        {
            return await _context.Categories.ToListAsync();
        }

        public async Task AddAsync(Companhia category)
        {
            await _context.Categories.AddAsync(category);
        }

        public async Task<Companhia> FindByIdAsync(int id)
        {
            return await _context.Categories.FindAsync(id);
        }

        public void Update(Companhia category)
        {
            _context.Categories.Update(category);
        }

        public void Remove(Companhia category)
        {
            _context.Categories.Remove(category);
        }


    }

}
