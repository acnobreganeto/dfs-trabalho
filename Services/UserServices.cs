﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Models;
using api_rest.Domain.Services;
using api_rest.Domain.Repositories;
using api_rest.Communication;

namespace api_rest.Services
{
    public class UserService : IUsuarioService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<Usuario> FirstOrDefaultAsync(String email, String senha)
        {
            return await _userRepository.FirstOrDefaultAsync(email, senha);
        }
    }

}
