﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Helpers;

namespace api_rest.Domain.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public float Valor { get; set; }
        public string Observacao { get; set; }

        public int CompanhiaId { get; set; }
        public Companhia Companhia { get; set; }
        public IList<Compra> Compras { get; set; } = new List<Compra>();
    }
}
