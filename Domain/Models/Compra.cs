﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Helpers;

namespace api_rest.Domain.Models
{
    public class Compra
    {
        public int Id { get; set; }
        public float Valor { get; set; }
        public DateTime Data { get; set; }
        public FormaPagamento FormaPagamento { get; set; }
        public Status StatusCompra { get; set; }
        public string Observacao { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
    }
}
