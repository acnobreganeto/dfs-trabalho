﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Domain.Models
{
        public class Companhia
        {
            public int Id { get; set; }
            public string NomeFantasia { get; set; }
            public string RazaoSocial { get; set; }
            public string Cnpj { get; set; }

            public IList<Produto> Products { get; set; } = new List<Produto>();
        }

}

