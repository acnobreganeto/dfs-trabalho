﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_rest.Domain.Models;

namespace api_rest.Domain.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Companhia>> ListAsync();
        Task AddAsync(Companhia companhia);
        Task<Companhia> FindByIdAsync(int id);
        void Update(Companhia companhia);
        void Remove(Companhia companhia);
    }
}
