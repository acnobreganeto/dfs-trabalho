﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Domain.Helpers
{
    public enum Status : byte
    {
        [Description("Compra Realizada")]
        Realizada = 1,

        [Description("Compra Cancelada")]
        Cancelada = 2
    }
}
