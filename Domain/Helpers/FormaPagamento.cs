﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Domain.Helpers
{
    public enum FormaPagamento : byte
    {
        [Description("Boleto Bancário")]
        Boleto = 1,

        [Description("Pix")]
        Pix = 2,

        [Description("Cartão de Crédito")]
        CartaoCredito = 3
    }
}
