﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using api_rest.Domain.Services;
using api_rest.Domain.Models;
using AutoMapper;
using api_rest.Resources;
using api_rest.Extensions;
using Microsoft.AspNetCore.Authorization;

namespace api_rest.Controllers
{
    [Route("/api/[controller]")]
    [Authorize()]
    public class CompanhiaController : Controller
    {

        private readonly ICompanhiaService _companhiaService;
        private readonly IMapper _mapper;

        public CompanhiaController(ICompanhiaService companhiaService, IMapper mapper)
        {
            _companhiaService = companhiaService;
            _mapper = mapper;
        }

        [HttpGet]

        public async Task<IEnumerable<CompanhiaResource>> GetAllAsync()
        {
            var companhias = await _companhiaService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Companhia>, IEnumerable<CompanhiaResource>>(companhias);

            return resources;
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] SaveCompanhiaResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var companhia = _mapper.Map<SaveCompanhiaResource, Companhia>(resource);
            var result = await _companhiaService.SaveAsync(companhia);

            if (!result.Success)
                return BadRequest(result.Message);

            var companhiaResource = _mapper.Map<Companhia, CompanhiaResource>(result.Companhia);

            return Ok(companhiaResource);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutAsync(int id, [FromBody] SaveCompanhiaResource resource)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState.GetErrorMessages());

            var companhia = _mapper.Map<SaveCompanhiaResource, Companhia>(resource);
            var result = await _companhiaService.UpdateAsync(id, companhia);

            if (!result.Success)
                return BadRequest(result.Message);

            var companhiaResource = _mapper.Map<Companhia, CompanhiaResource>(result.Companhia);
            return Ok(companhiaResource);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            var result = await _companhiaService.DeleteAsync(id);

            if (!result.Success)
                return BadRequest(result.Message);

            var companhiaResource = _mapper.Map<Companhia, CompanhiaResource>(result.Companhia);
            return Ok(companhiaResource);
        }

    }

}
