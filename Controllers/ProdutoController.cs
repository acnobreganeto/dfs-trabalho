﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using api_rest.Domain.Models;
using api_rest.Domain.Services;
using api_rest.Resources;
using Microsoft.AspNetCore.Authorization;

namespace api_rest.Controllers
{
    [Route("/api/[controller]")]
    [Authorize()]
    public class ProdutoController : Controller
    {
        private readonly IProdutoService _productService;
        private readonly IMapper _mapper;

        public ProdutoController(IProdutoService productService, IMapper mapper)
        {
            _productService = productService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<ProdutoResource>> ListAsync()
        {
            var products = await _productService.ListAsync();
            var resources = _mapper.Map<IEnumerable<Produto>, IEnumerable<ProdutoResource>>(products);
            return resources;
        }
    }
}
