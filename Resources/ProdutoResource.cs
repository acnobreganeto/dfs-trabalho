﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Resources
{
    public class ProdutoResource
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int QuantityInPackage { get; set; }
        public string UnitOfMeasurement { get; set; }
        public CompanhiaResource Companhia { get; set; }

    }
}
