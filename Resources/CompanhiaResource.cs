﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Resources
{
    public class CompanhiaResource
    {
        public int Id { get; set; }
        public string NomeFantasia { get; set; }
    }
}
