﻿using api_rest.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api_rest.Communication
{
    public class CategoryResponse : BaseResponse
    {
        public Companhia Companhia { get; private set; }

        private CategoryResponse(bool success, string message, Companhia companhia) : base(success, message)
        {
            Companhia = companhia;
        }

        /// <summary>
        /// Creates a success response.
        /// </summary>
        /// <param name="companhia">Saved companhia.</param>
        /// <returns>Response.</returns>
        public CategoryResponse(Companhia companhia) : this(true, string.Empty, companhia)
        { }

        /// <summary>
        /// Creates am error response.
        /// </summary>
        /// <param name="message">Error message.</param>
        /// <returns>Response.</returns>
        public CategoryResponse(string message) : this(false, message, null)
        { }
    }
}
